import argparse
from random import choice

from PIL import Image, ImageDraw


class MazeCell:
    def __init__(self):
        self.wall_n = True
        self.wall_s = True
        self.wall_w = True
        self.wall_e = True

        self.g = 0
        self.h = 0
        self.f = 0
        self.parent = None

    def __unicode__(self):
        """
        Prints a representation of the cell's state using Unicode box characters
        for the sake of debugging
        """
        walls = u'\u250f%s\u2513\n' % (u'\u2501' * 3 if self.wall_n else '   ')
        walls += u'\u2503' if self.wall_w else ' '
        walls += '   '
        walls += u'\u2503\n' if self.wall_e else ' \n'
        walls += u'\u2517%s\u251b' % (u'\u2501' * 3 if self.wall_s else '   ')
        return walls

    def __str__(self):
        return unicode(self).encode('utf-8')


class Maze:
    def __init__(self, width, height):
        """
        Prepares an empty maze grid (all cells have no paths) in self.maze
        and then calls self.carve to create an initial maze
        """
        self.width = width
        self.height = height
        self.maze = [[MazeCell() for i in range(height)] for i in range(width)]
        self.carve()

    def __getitem__(self, index):
        return self.maze[index]

    def __iter__(self):
        return self.maze.__iter__()

    def cell_is_in_bounds(self, x, y):
        """
        Return whether or not the given coordinates fall inside the bounds of
        the maze.
        """
        if x < 0 or x >= self.width:
            return False
        if y < 0 or y >= self.height:
            return False
        return True

    def carve(self):
        """
        Visits each cell and carves the paths for the maze
        """
        visited = []
        stack = []

        current_cell = (0, 0)
        visited.append(current_cell)
        # Keep going until we've visted every cell once
        while len(visited) < self.width * self.height:
            x, y = current_cell
            neighbors = [(x - 1, y), (x, y + 1), (x + 1, y), (x, y - 1)]
            valid_neighbors = [neighbor for neighbor in neighbors 
                               if self.cell_is_in_bounds(*neighbor) and
                               neighbor not in visited]
            if valid_neighbors:
                # If we have one or more neighbors, we can carve into one of
                # them at random to extend our path
                next_cell = choice(valid_neighbors)
                stack.append(next_cell)
                xx, yy = next_cell
                # Open an exit on the current cell and a corresponding entrance
                # on the next cell
                if xx > x:
                    self.maze[x][y].wall_e = False
                    self.maze[xx][yy].wall_w = False
                elif xx < x:
                    self.maze[x][y].wall_w = False
                    self.maze[xx][yy].wall_e = False
                elif yy > y:
                    self.maze[x][y].wall_s = False
                    self.maze[xx][yy].wall_n = False
                else:
                    self.maze[x][y].wall_n = False
                    self.maze[xx][yy].wall_s = False
                current_cell = next_cell
                visited.append(current_cell)
            else:
                # If there are no valid members, backtrack through our stack
                # until we find a cell that has a neighbor we can carve into
                current_cell = stack.pop()

    def solve(self, start, end):
        """
        Uses A* pathfinding algorithm to find the shortest route to the end of
        the maze
        """
        def process_cell(new, current, cell):
            x, y = new
            c = self.maze[x][y]
            if new not in open_list:
                open_list.append(new)
                c.parent = current
                c.g = cell.g + 1
                c.h = abs(end[0] - x) + abs(end[1] - y)
                c.f = c.g + c.h
            else:
                if cell.g + 1 < c.g:
                    c.parent = current
                    c.g += 1
                    c.f = c.g + c.h

        if start == end:
            return [start]

        open_list = [start]
        closed_list = []
        current_xy = open_list.pop(0)

        while current_xy != end:
            a, b = current_xy
            current_cell = self.maze[a][b]
            if not current_cell.wall_n:
                new_cell = (a, b - 1)
                if new_cell not in closed_list:
                    process_cell(new_cell, current_xy, current_cell)

            if not current_cell.wall_s:
                new_cell = (a, b + 1)
                if new_cell not in closed_list:
                    process_cell(new_cell, current_xy, current_cell)

            if not current_cell.wall_e:
                new_cell = (a + 1, b)
                if new_cell not in closed_list:
                    process_cell(new_cell, current_xy, current_cell)

            if not current_cell.wall_w:
                new_cell = (a - 1, b)
                if new_cell not in closed_list:
                    process_cell(new_cell, current_xy, current_cell)

            current_xy = open_list.pop(0)
            closed_list.append(current_xy)
            open_list.sort(key=lambda z: self.maze[z[0]][z[1]].f)

        current = end
        solution = [start]
        while current != start:
            solution.append(current)
            i, j = current
            cell = self.maze[i][j]
            current = cell.parent

        return solution

    def draw_png(self, grid_size=16):
        img = Image.new('RGB', (self.width * grid_size + grid_size / 2,
                                self.height * grid_size + grid_size / 2))
        draw = ImageDraw.Draw(img)
        white = (255, 255, 255)
        wall_size = grid_size / 4

        for x, col in enumerate(maze):
            for y, cell in enumerate(col):
                dx, dy = x * grid_size + wall_size, y * grid_size + wall_size

                # Draw white rectangles to open up any passages we carved
                if not cell.wall_n:
                    draw.rectangle([(dx + wall_size,
                                     dy),
                                    (dx + wall_size * 3 - 1,
                                     dy + wall_size * 3 - 1)],
                                   fill=white)
                if not cell.wall_s:
                    draw.rectangle([(dx + wall_size,
                                     dy + wall_size),
                                    (dx + wall_size * 3 - 1,
                                     dy + grid_size)],
                                   fill=white)
                if not cell.wall_w:
                    draw.rectangle([(dx,
                                     dy + wall_size),
                                    (dx + wall_size * 3 - 1,
                                     dy + wall_size * 3 - 1)],
                                   fill=white)
                if not cell.wall_e:
                    draw.rectangle([(dx + wall_size,
                                     dy + wall_size),
                                    (dx + grid_size,
                                     dy + wall_size * 3 - 1)],
                                   fill=white)

        # Open the upper left and lower right to create start and exit
        draw.rectangle([(0,
                         wall_size * 2),
                        (wall_size * 2,
                         grid_size - 1)],
                       fill=white)
        draw.rectangle([(self.width * grid_size,
                         self.height * grid_size - grid_size / 2),
                        (self.width * grid_size + grid_size / 2,
                         self.height * grid_size - 1)],
                       fill=white)

        return img

    def export_png(self, filename='maze.png', grid_size=16):
        """
        Export a PNG representation of the maze
        """
        img = self.draw_png(grid_size)
        img.save(filename)

    def export_solution(self, filename='maze-solution.png', grid_size=16):
        """
        Export a PNG representation of the maze with the shortest path
        highlighted in red
        """
        solution = self.solve((0, 0), (self.width - 1, self.height - 1))
        img = self.draw_png(grid_size)
        red = (255, 0, 0)
        wall_size = grid_size / 4

        draw = ImageDraw.Draw(img)

        for x, y in solution:
            cell = maze[x][y]
            dx, dy = x * grid_size + wall_size, y * grid_size + wall_size
            if cell.parent is None:
                draw.rectangle([(dx + wall_size,
                                 dy + wall_size),
                                (dx + wall_size * 3 - 1,
                                 dy + wall_size * 3 - 1)],
                               fill=red)
            elif cell.parent[1] < y:
                draw.rectangle([(dx + wall_size,
                                 dy - wall_size),
                                (dx + wall_size * 3 - 1,
                                 dy + wall_size * 3 - 1)],
                               fill=red)
            elif cell.parent[1] > y:
                draw.rectangle([(dx + wall_size,
                                 dy + wall_size),
                                (dx + wall_size * 3 - 1,
                                 dy + grid_size + wall_size)],
                               fill=red)
            elif cell.parent[0] < x:
                draw.rectangle([(dx - wall_size,
                                 dy + wall_size),
                                (dx + wall_size * 3 - 1,
                                 dy + wall_size * 3 - 1)],
                               fill=red)
            elif cell.parent[0] > x:
                draw.rectangle([(dx + wall_size,
                                 dy + wall_size),
                                (dx + grid_size + wall_size,
                                 dy + wall_size * 3 - 1)],
                               fill=red)

        # Highlight the upper left and lower right to create start and exit
        draw.rectangle([(0,
                         wall_size * 2),
                        (wall_size * 2 - 1,
                         grid_size - 1)],
                       fill=red)
        draw.rectangle([(self.width * grid_size,
                         self.height * grid_size - grid_size / 2),
                        (self.width * grid_size + grid_size / 2,
                         self.height * grid_size - 1)],
                       fill=red)
        img.save(filename)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-x', '--width', type=int, default=20)
    parser.add_argument('-y', '--height', type=int, default=20)
    parser.add_argument('-f', '--filename', default='maze.png')
    parser.add_argument('-g', '--gridsize', type=int, default=16)
    parser.add_argument('--solve', action="store_true")
    parser.add_argument('--solvefile', default='maze-solved.png')
    args = parser.parse_args()
    maze = Maze(args.width, args.height)
    maze.export_png(filename=args.filename, grid_size=args.gridsize)
    if args.solve:
        maze.export_solution(filename=args.solvefile, grid_size=args.gridsize)
